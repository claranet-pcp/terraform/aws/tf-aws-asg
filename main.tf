/**
 * # tf-aws-asg
 *
 * AWS Autoscaling group Terraform Module.
 *
 * Built loosely as a replacement for `autoscaling.json` used in CloudFormation in antiquity.
 *
 * Gives you:
 *
 * - An autoscaling group
 * - A launch config
 * - Optionally a CPU based policy to scale up/down
 *
 * It is suggested you use `tf-aws-sg` and `tf-aws-iam-instance-profile` in conjunction with this module.
 * You can create one or more ELBs with `tf-aws-elb`, and pass a list of load balancers in.
 *
 * ## Terraform version compatibility
 *
 * | Module version | Terraform version |
 * |----------------|-------------------|
 * | 2.x.x          | 0.13.x            |
 * | 1.x.x          | 0.12.x            |
 * | 0.x.x          | 0.11.x            |
 *
 * ## Contributing
 *
 * Ensure any variables you add have a type and a description.
 *
 * ## Usage
 *
 * ```data "template_file" "cc" {
 *   template = file("templates/cloud-config.cfg.tpl")
 * }
 *
 * data "template_file" "script" {
 *   template = file("templates/boot.sh.tpl")
 * }
 *
 * data "template_cloudinit_config" "config" {
 *   gzip          = true
 *   base64_encode = true
 *
 *   part {
 *     content_type = "text/cloud-config"
 *     content      = data.template_file.cc.rendered
 *   }
 *
 *   part {
 *     content_type = "text/x-shellscript"
 *     content      = data.template_file.script.rendered
 *   }
 * }
 *
 * module "asg" {
 *   source    = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-aws-asg.git"
 *   name      = "my-asg"
 *   envname   = "test"
 *   service   = "varnish"
 *   ami_id    = "ami-a85165db"
 *   user_data = data.template_cloudinit_config.config.rendered
 * }
 * ```
 *
 * ## Testing
 * 
 * Run `make test` in the tests/ directory
 *
 * ## TODO
 *
 * - Proper automated tests
 *
*/