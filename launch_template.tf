// Launch Template

resource "aws_launch_template" "lt" {
  count = var.enabled && var.enable_attach_eni ? 0 : 1

  lifecycle {
    create_before_destroy = true
  }

  name_prefix   = var.name
  image_id      = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_name
  user_data     = base64encode(var.user_data)

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = var.associate_public_ip_address
    security_groups             = var.security_groups
  }

  metadata_options {
    http_tokens                   = var.metadata_http_tokens
    http_endpoint                 = var.metadata_http_endpoint
    http_put_response_hop_limit   = var.metadata_http_put_response_hop_limit
    instance_metadata_tags        = var.metadata_instance_tags
  }

  dynamic "block_device_mappings" {
    for_each = var.block_device_mappings
    content {
      device_name  = block_device_mappings.value.device_name
      no_device    = try(block_device_mappings.value.no_device, null)
      virtual_name = try(block_device_mappings.value.virtual_name, null)

      dynamic "ebs" {
        for_each = flatten([try(block_device_mappings.value.ebs, [])])
        content {
          delete_on_termination = try(ebs.value.delete_on_termination, null)
          encrypted             = try(ebs.value.encrypted, null)
          iops                  = try(ebs.value.iops, null)
          volume_size           = try(ebs.value.volume_size, null)
          volume_type           = try(ebs.value.volume_type, null)
          snapshot_id           = try(ebs.value.snapshot_id, null)
        }
      }

    }
  }

}

resource "aws_launch_template" "lt_attach_eni" {
  count = var.enabled && var.enable_attach_eni ? 1 : 0

  lifecycle {
    create_before_destroy = true
  }

  name_prefix   = var.name
  image_id      = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_name
  user_data     = base64encode(var.user_data)

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  monitoring {
    enabled = true
  }

  network_interfaces {
    device_index         = 0
    network_interface_id = aws_network_interface.lt_ni[0].id
  }

  dynamic "block_device_mappings" {
    for_each = var.block_device_mappings
    content {
      device_name  = block_device_mappings.value.device_name
      no_device    = try(block_device_mappings.value.no_device, null)
      virtual_name = try(block_device_mappings.value.virtual_name, null)

      dynamic "ebs" {
        for_each = flatten([try(block_device_mappings.value.ebs, [])])
        content {
          delete_on_termination = try(ebs.value.delete_on_termination, null)
          encrypted             = try(ebs.value.encrypted, null)
          iops                  = try(ebs.value.iops, null)
          volume_size           = try(ebs.value.volume_size, null)
          volume_type           = try(ebs.value.volume_type, null)
          snapshot_id           = try(ebs.value.snapshot_id, null)
        }
      }

    }
  }

}

resource "aws_network_interface" "lt_ni" {
  count                   = var.enabled && var.enable_attach_eni ? 1 : 0
  private_ip_list         = var.eni_private_ip_list
  subnet_id               = var.eni_subnet_id
  private_ip_list_enabled = true
  security_groups         = var.security_groups
}