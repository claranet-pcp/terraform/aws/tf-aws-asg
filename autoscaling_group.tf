resource "aws_autoscaling_group" "asg" {
  count = var.enabled && var.enable_attach_eni ? 0 : 1

  name                = var.name
  vpc_zone_identifier = var.subnets # A list of subnet IDs to launch resources in.

  load_balancers    = var.load_balancers
  target_group_arns = var.target_group_arns

  min_size             = var.min
  max_size             = var.max
  default_cooldown     = var.cooldown
  termination_policies = var.termination_policies

  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type

  enabled_metrics = var.enabled_metrics

  suspended_processes = var.suspended_processes

  launch_template {
    id      = aws_launch_template.lt[count.index].id
    version = "$Latest"
  }

  dynamic "tag" {
    for_each = local.tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg_attach_eni" {
  count = var.enabled && var.enable_attach_eni ? 1 : 0

  name               = var.name
  availability_zones = var.eni_availability_zones # A list of one or more availability zones for the group. Used for private IP allocation.

  load_balancers    = var.load_balancers
  target_group_arns = var.target_group_arns

  min_size             = var.min
  max_size             = var.max
  default_cooldown     = var.cooldown
  termination_policies = var.termination_policies

  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type

  enabled_metrics = var.enabled_metrics

  suspended_processes = var.suspended_processes

  launch_template {
    id      = aws_launch_template.lt_attach_eni[count.index].id
    version = "$Latest"
  }

  dynamic "tag" {
    for_each = local.tags
    content {
      key                 = tag.value.key
      value               = tag.value.value
      propagate_at_launch = tag.value.propagate_at_launch
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

locals {
  default_tags = [
    {
      key                 = "Name"
      value               = var.name
      propagate_at_launch = true
    },
    {
      key                 = "Environment"
      value               = var.envname
      propagate_at_launch = true
    },
    {
      key                 = "Service"
      value               = var.service
      propagate_at_launch = true
    }
  ]

  patch_group_tag = [
    {
      key                 = "Patch Group"
      value               = var.patch_group
      propagate_at_launch = true
    }
  ]

  tags = concat(
    var.use_default_tags ? local.default_tags : [],
    var.patch_group != "" ? local.patch_group_tag : [],
    var.extra_tags
  )
}
