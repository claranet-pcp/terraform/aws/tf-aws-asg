# tf-aws-asg

AWS Autoscaling group Terraform Module.

Built loosely as a replacement for `autoscaling.json` used in CloudFormation in antiquity.

Gives you:

- An autoscaling group
- A launch config
- Optionally a CPU based policy to scale up/down

It is suggested you use `tf-aws-sg` and `tf-aws-iam-instance-profile` in conjunction with this module.
You can create one or more ELBs with `tf-aws-elb`, and pass a list of load balancers in.

## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 2.x.x          | 0.13.x            |
| 1.x.x          | 0.12.x            |
| 0.x.x          | 0.11.x            |

## Contributing

Ensure any variables you add have a type and a description.

## Usage

```data "template_file" "cc" {
  template = file("templates/cloud-config.cfg.tpl")
}

data "template_file" "script" {
  template = file("templates/boot.sh.tpl")
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = data.template_file.cc.rendered
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.script.rendered
  }
}

module "asg" {
  source    = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-aws-asg.git"
  name      = "my-asg"
  envname   = "test"
  service   = "varnish"
  ami_id    = "ami-a85165db"
  user_data = data.template_cloudinit_config.config.rendered
}
```

## Testing

Run `make test` in the tests/ directory

## TODO

- Proper automated tests

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_group.asg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) | resource |
| [aws_autoscaling_group.asg_attach_eni](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_group) | resource |
| [aws_autoscaling_policy.scale_policy_down](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_autoscaling_policy.scale_policy_target_tracking](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_autoscaling_policy.scale_policy_up](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_cloudwatch_metric_alarm.scale_alarm_down](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.scale_alarm_up](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_launch_template.lt](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) | resource |
| [aws_launch_template.lt_attach_eni](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template) | resource |
| [aws_network_interface.lt_ni](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_interface) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | The AMI used to create ASG instances | `string` | n/a | yes |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Associate a public ip address with an ASG launched instance | `string` | `false` | no |
| <a name="input_autoscaling"></a> [autoscaling](#input\_autoscaling) | Bool indicating whether to create Autoscale Policies | `string` | `false` | no |
| <a name="input_block_device_mappings"></a> [block\_device\_mappings](#input\_block\_device\_mappings) | Configure additional volumes of the instance besides specificed by the AMI | `list(any)` | `[]` | no |
| <a name="input_cooldown"></a> [cooldown](#input\_cooldown) | The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start | `string` | `300` | no |
| <a name="input_cpu_scale_down"></a> [cpu\_scale\_down](#input\_cpu\_scale\_down) | The value against which the CPU usage is compared to decide scale down action | `string` | `"20"` | no |
| <a name="input_cpu_scale_up"></a> [cpu\_scale\_up](#input\_cpu\_scale\_up) | The value against which the CPU usage is compared to decide scale up action | `string` | `"60"` | no |
| <a name="input_detailed_monitoring"></a> [detailed\_monitoring](#input\_detailed\_monitoring) | Enables/disables detailed monitoring | `string` | `true` | no |
| <a name="input_ebs_block_device"></a> [ebs\_block\_device](#input\_ebs\_block\_device) | Additional EBS block devices to attach to the instance | `list(any)` | `[]` | no |
| <a name="input_enable_attach_eni"></a> [enable\_attach\_eni](#input\_enable\_attach\_eni) | Attach ENI to ASG launched instance, for use with private IP allocation use cases | `bool` | `false` | no |
| <a name="input_enabled"></a> [enabled](#input\_enabled) | Enable or disable the ASG.  The functionality has been added to the resource within the module as counts cannot be used on a module itself. | `string` | `"1"` | no |
| <a name="input_enabled_metrics"></a> [enabled\_metrics](#input\_enabled\_metrics) | A list of metrics to collect. The allowed values are GroupMinSize, GroupMaxSize, GroupDesiredCapacity, GroupInServiceInstances, GroupPendingInstances, GroupStandbyInstances, GroupTerminatingInstances, GroupTotalInstances | `list(string)` | <pre>[<br>  "GroupMinSize",<br>  "GroupMaxSize",<br>  "GroupDesiredCapacity",<br>  "GroupInServiceInstances",<br>  "GroupPendingInstances",<br>  "GroupStandbyInstances",<br>  "GroupTerminatingInstances",<br>  "GroupTotalInstances"<br>]</pre> | no |
| <a name="input_eni_availability_zones"></a> [eni\_availability\_zones](#input\_eni\_availability\_zones) | List of one or more availability zones for the group. Used for EC2-Classic, attaching a network interface via id from a launch template | `list(any)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_eni_private_ip_list"></a> [eni\_private\_ip\_list](#input\_eni\_private\_ip\_list) | List of private IPs to assign to the ENI in sequential order | `list(any)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_eni_subnet_id"></a> [eni\_subnet\_id](#input\_eni\_subnet\_id) | Subnet ID to create the ENI in | `string` | `""` | no |
| <a name="input_envname"></a> [envname](#input\_envname) | This will become the value for the 'Environment' tag on resources created by this module | `string` | n/a | yes |
| <a name="input_ephemeral_block_device"></a> [ephemeral\_block\_device](#input\_ephemeral\_block\_device) | Customize Ephemeral (also known as 'Instance Store') volumes on the instance | `list(any)` | `[]` | no |
| <a name="input_extra_tags"></a> [extra\_tags](#input\_extra\_tags) | A list of extra tags for the ASG | `list(map(any))` | `[]` | no |
| <a name="input_health_check_grace_period"></a> [health\_check\_grace\_period](#input\_health\_check\_grace\_period) | Time (seconds) after instance comes into service before checking health | `string` | `300` | no |
| <a name="input_health_check_type"></a> [health\_check\_type](#input\_health\_check\_type) | This value controls how health checking is done | `string` | `"EC2"` | no |
| <a name="input_iam_instance_profile"></a> [iam\_instance\_profile](#input\_iam\_instance\_profile) | The IAM instance profile to associate with launched instances | `string` | `""` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | The instance type for the ASG to create | `string` | `"t2.micro"` | no |
| <a name="input_key_name"></a> [key\_name](#input\_key\_name) | The key name that should be used for the ASG launched instances | `string` | `"bashton"` | no |
| <a name="input_load_balancers"></a> [load\_balancers](#input\_load\_balancers) | A list of Elastic Load Balancer names to add to the autoscaling group names | `list(string)` | `[]` | no |
| <a name="input_max"></a> [max](#input\_max) | The maximum size of the Autoscale Group | `string` | `1` | no |
| <a name="input_min"></a> [min](#input\_min) | The minimum size of the Autoscale Group | `string` | `1` | no |
| <a name="input_name"></a> [name](#input\_name) | The desired name prefix for your ASG resources. Will also be added as the value for the 'Name' tag | `string` | n/a | yes |
| <a name="input_patch_group"></a> [patch\_group](#input\_patch\_group) | Adds a 'Patch Group' tag to the ASG with this value | `string` | `""` | no |
| <a name="input_root_block_device"></a> [root\_block\_device](#input\_root\_block\_device) | Customize details about the root block device of the instance | `list(any)` | `[]` | no |
| <a name="input_scale_factor_down"></a> [scale\_factor\_down](#input\_scale\_factor\_down) | The number of instances by which to scale down by at a time | `string` | `"-1"` | no |
| <a name="input_scale_factor_up"></a> [scale\_factor\_up](#input\_scale\_factor\_up) | The number of instances by which to scale up by at a time | `string` | `"1"` | no |
| <a name="input_scale_minutes_down"></a> [scale\_minutes\_down](#input\_scale\_minutes\_down) | How many minutes should pass before a scale down event should occur if the cpu\_scale\_up threshold is no longer exceeded | `string` | `"20"` | no |
| <a name="input_scale_minutes_up"></a> [scale\_minutes\_up](#input\_scale\_minutes\_up) | How many minutes should pass before a scale up event should occur if the cpu\_scale\_up threshold is exceeded | `string` | `"5"` | no |
| <a name="input_scale_statistic"></a> [scale\_statistic](#input\_scale\_statistic) | The statistic to apply to the Autoscale alarm's associated metric | `string` | `"Maximum"` | no |
| <a name="input_scaling_policy_type"></a> [scaling\_policy\_type](#input\_scaling\_policy\_type) | Autoscaling strategy | `string` | `"SimpleScaling"` | no |
| <a name="input_security_groups"></a> [security\_groups](#input\_security\_groups) | A list of associated security group IDS | `list(string)` | `[]` | no |
| <a name="input_service"></a> [service](#input\_service) | This will become the value for the 'Service' tag on resources created by this module | `string` | n/a | yes |
| <a name="input_subnets"></a> [subnets](#input\_subnets) | A list of subnet IDs to launch resources in | `list(string)` | <pre>[<br>  ""<br>]</pre> | no |
| <a name="input_suspended_processes"></a> [suspended\_processes](#input\_suspended\_processes) | List of processes to suspend for the ASG | `list(string)` | `[]` | no |
| <a name="input_target_group_arns"></a> [target\_group\_arns](#input\_target\_group\_arns) | A list of aws\_alb\_target\_group ARNs, for use with Application Load Balancing | `list(string)` | `[]` | no |
| <a name="input_target_tracking_target_cpu"></a> [target\_tracking\_target\_cpu](#input\_target\_tracking\_target\_cpu) | Bool indicating whether to create target tracking scaling policy | `string` | `"60"` | no |
| <a name="input_termination_policies"></a> [termination\_policies](#input\_termination\_policies) | A list of policies to decide how the instances in the auto scale group should be terminated | `list(string)` | <pre>[<br>  "OldestLaunchConfiguration",<br>  "ClosestToNextInstanceHour"<br>]</pre> | no |
| <a name="input_use_default_tags"></a> [use\_default\_tags](#input\_use\_default\_tags) | Bool indicating whether to apply default tags | `bool` | `true` | no |
| <a name="input_user_data"></a> [user\_data](#input\_user\_data) | The user data to provide when launching the instance | `string` | `""` | no |
| <a name="input_warmup_seconds"></a> [warmup\_seconds](#input\_warmup\_seconds) | The estimated time, in seconds, until a newly launched instance will contribute CloudWatch metrics | `string` | `"60"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_asg_arn"></a> [asg\_arn](#output\_asg\_arn) | Autoscaling group ARN |
| <a name="output_asg_id"></a> [asg\_id](#output\_asg\_id) | Autoscaling group id |
| <a name="output_asg_name"></a> [asg\_name](#output\_asg\_name) | Autoscaling group name |
| <a name="output_launch_template_id"></a> [launch\_template\_id](#output\_launch\_template\_id) | Launch template id |
