output "launch_template_id" {
  description = "Launch template id"
  value       = join("", try(aws_launch_template.lt.*.id, aws_launch_template.lt_attach_eni.*.id))
}

output "asg_id" {
  description = "Autoscaling group id"
  value       = join("", try(aws_autoscaling_group.asg.*.id, aws_autoscaling_group.asg_attach_eni.*.id))
}

output "asg_name" {
  description = "Autoscaling group name"
  value       = join("", try(aws_autoscaling_group.asg.*.name, aws_autoscaling_group.asg_attach_eni.*.name))
}

output "asg_arn" {
  description = "Autoscaling group ARN"
  value       = join("", try(aws_autoscaling_group.asg.*.arn, aws_autoscaling_group.asg_attach_eni.*.arn))
}
